DESCRIPTION = "Pramfs init scripts"
SECTION = "init"
LICENSE = "BSD"
SRC_URI = "file://pramfs_init"
PR = "r0"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PACKAGE_ARCH = "all"

do_install() {
	install -d ${D}${sysconfdir}/init.d/
	install -m 0755 ${WORKDIR}/pramfs_init ${D}${sysconfdir}/init.d/pramfs_init
}

inherit update-rc.d

INITSCRIPT_NAME = "pramfs_init"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 19 0 1 6 ."
