RRECOMMENDS_${PN} += "pramfs-init"

PRAMFS_VERSION = '1.5.1-3.12'

SRC_URI += "file://pramfs-${PRAMFS_VERSION}.tar.gz \
            file://cfg/pramfs.cfg"

STAGING_KERNEL_FEATURES_append = " cfg/pramfs.cfg"

do_apply_pramfs() {
    # Apply PRAMFS patch

    # Already done by bitbake ->
    # tar xvf ${WORKDIR}/pramfs-${PRAMFS_VERSION}.tar.gz -C ${WORKDIR}
    cd ${WORKDIR}/pramfs-${PRAMFS_VERSION}/
    ./patch-ker.sh ${S}
    cd -
}

do_apply_pramfs[deptask] = "do_unpack"
addtask apply_pramfs after do_unpack before do_patch
