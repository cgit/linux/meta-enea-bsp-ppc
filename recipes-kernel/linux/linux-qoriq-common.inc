require recipes-kernel/linux/enea-common.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://b4860-hard_irq_disable-bug.patch \
    file://0001-sdhci-fix-Timeout-error-messages.patch \
    "

ENEA_KERNEL_FRAGMENTS += "\
    cfg/localversion.cfg \
    cfg/with_modules.cfg \
    cfg/embedded.cfg \
    cfg/preempt.cfg \
    cfg/root_nfs.cfg \
    cfg/devtmpfs.cfg \
    cfg/bootlogd.cfg \
    cfg/mtd_tests.cfg \
    cfg/latencytop.cfg \
    cfg/ltp.cfg \
    cfg/fuse.cfg \
    cfg/dpa.cfg \
    cfg/kprobes.cfg \
    cfg/i2c.cfg \
    cfg/lttng.cfg \
    cfg/powertop.cfg \
    cfg/systemtap.cfg \
    cfg/kgdb.cfg \
    cfg/gpio.cfg \
    cfg/cpusets.cfg \
    cfg/nfsdv3.cfg \
    cfg/nfsdv4.cfg \
    cfg/nls_cp437.cfg \
    "

ENEA_KERNEL_FRAGMENTS_append_p2020rdb=" \
    cfg/uio.cfg \
    "

DELTA_KERNEL_DEFCONFIG += " ${ENEA_KERNEL_FRAGMENTS} \
    "

require recipes-kernel/linux/pramfs-3.12.inc

