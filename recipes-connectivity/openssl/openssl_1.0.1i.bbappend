FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://Makefiles-ptest.patch \
            file://ptest-deps.patch \
            file://run-ptest \
           "

DEPENDS_append_class-target = " openssl-native"

inherit ptest

RDEPENDS_${PN}-ptest += "${PN}-misc make perl perl-module-filehandle bc"

do_compile_prepend_class-target () {
    sed -i 's/\((OPENSSL=\)".*"/\1"openssl"/' Makefile
}

do_compile_ptest () {
       oe_runmake buildtest
}

do_install_ptest () {
       cp -r Makefile test ${D}${PTEST_PATH}
       cp -r certs ${D}${PTEST_PATH}
       mkdir -p ${D}${PTEST_PATH}/apps
       ln -sf /usr/lib/ssl/misc/CA.sh  ${D}${PTEST_PATH}/apps
       ln -sf /usr/lib/ssl/openssl.cnf ${D}${PTEST_PATH}/apps
       ln -sf /usr/bin/openssl         ${D}${PTEST_PATH}/apps
       cp apps/server2.pem             ${D}${PTEST_PATH}/apps
       mkdir -p ${D}${PTEST_PATH}/util
       install util/opensslwrap.sh    ${D}${PTEST_PATH}/util
       install util/shlib_wrap.sh     ${D}${PTEST_PATH}/util
}

do_install_append_virtclass-native() {
       create_wrapper ${D}${bindir}/openssl \
           OPENSSL_CONF=${libdir}/ssl/openssl.cnf \
           SSL_CERT_DIR=${libdir}/ssl/certs \
           SSL_CERT_FILE=${libdir}/ssl/cert.pem \
           OPENSSL_ENGINES=${libdir}/ssl/engines
}
