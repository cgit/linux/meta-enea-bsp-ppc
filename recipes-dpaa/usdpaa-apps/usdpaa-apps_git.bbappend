# Remove RGMII port since it is allocated to Linux stack when boot with -usdpaa-enea- device tree
# Removed port must always match with the one removed from -usdpaa-enea-

do_configure_prepend() {
    sed '/type=\"MAC\" number=\"5\"/d' ${S}/src/ppac/usdpaa_config_p2_p3_p5_14g.xml > ${S}/src/ppac/usdpaa_config_enea_p2_p3_p5_14g.xml
    sed '/usdpaa_config_p2_p3_p5_14g\.xml/a\        usdpaa_config_enea_p2_p3_p5_14g.xml             \\'  ${S}/src/ppac/Makefile.am > ${S}/src/ppac/Makefile.am.bak && mv ${S}/src/ppac/Makefile.am.bak ${S}/src/ppac/Makefile.am
}
